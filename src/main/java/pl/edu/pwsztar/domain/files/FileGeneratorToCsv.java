package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.util.List;

public interface FileGeneratorToCsv {
    InputStreamResource toCsv(List<FileDto> fileDtoList);
}
