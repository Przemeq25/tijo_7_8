package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.IOException;
import java.util.List;


// TODO: Czy interfejs nie lamie zasady SOLID?
public interface FileGeneratorToTxt {

    InputStreamResource toTxt(List<FileDto>fileDtoList) throws IOException;

}
