package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.files.FileGeneratorToTxt;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.FileService;

import java.io.*;
import java.util.List;
@Service
public class FileServiceImpl implements FileGeneratorToTxt, FileService {

    private final MovieRepository movieRepository;
    private final Converter<List<Movie>, List<FileDto>> listConverter;


    @Autowired
    public FileServiceImpl(MovieRepository movieRepository,
                           Converter<List<Movie>, List<FileDto>> listConverter){
        this.movieRepository = movieRepository;
        this.listConverter = listConverter;
    }
    @Override
    public void addMovieToFile(List<FileDto> fileDtoList, File file) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(file);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        for(FileDto fileDto : fileDtoList){
            bufferedWriter.write(fileDto.getYear() + " " + fileDto.getTitle());
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        outputStream.flush();
        outputStream.close();
    }
    @Override
    public List<FileDto> getMoviesSorted() {
        return listConverter.convert(movieRepository.findAll(Sort.by("year").descending()));
    }

    @Override
    public InputStreamResource getInputStreamResource(File file) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(file);
        return new InputStreamResource(inputStream);
    }
    @Override
    public InputStreamResource toTxt(List<FileDto> fileDtoList) throws IOException {
        File file=File.createTempFile("tmp", ".txt");
        addMovieToFile(fileDtoList, file);
        return getInputStreamResource(file);
    }
}
